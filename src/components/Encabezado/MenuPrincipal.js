import React from 'react';
import {Link} from 'react-router-dom';
import img_stilo from '../css/img_stilo/menuPrincipal.css'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import PrimeraVista from '../PrimeraVista/PrimeraVista'



const MenuPrincipal = () => {


  return (

    <div className="container ">

      <div className="row ">
        <div>
          <ul className="nav">
            <li><Link to="">Inicio</Link>
              <ul className="sub">
              </ul>
            </li>

            <li><Link to="resultados">Resultados</Link>
                
              <ul className="sub">
                <li><a href="#">Temporada</a></li>
                <li><a href="#">Posiciones</a></li>
                <li><a href="#">Partidos Previos</a></li>
              </ul>
            </li>

            <li><Link to="proxpartidos">Próx. Partidos</Link>
              <ul className="sub">
              </ul>
            </li>

            <li><Link to="organizador">Organizador</Link>
              <ul className="sub">
              </ul>
            </li>

            <li><Link to="acercade">Acerca de</Link>
              <ul className="sub">
              </ul>
            </li>

          </ul>
        </div>
      </div>
    </div>



  );
}

export default MenuPrincipal;