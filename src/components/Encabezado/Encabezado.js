import React from 'react';

import Login from './Login';
import Buscador from './Buscador';


import MenuPrincipal from './MenuPrincipal';
import SimpleSlider from './SimpleSlider';


export function Encabezado() {



  var a = 1;
  return (



    <div className="container rounded">


      <div className="row bg-dark text-light rounded-top  ">
        <div className="col mt-0 ml-5 " align="center ">
          <h1 align="center " >

          
                  Deportes ITP
               
         </h1>
        </div>

        <div className="col ">
          {a == 1 ? <Login /> : "Conectado"}
        </div>
      </div>

      <div className="row  bg-dark rounded-bottom">
        <div className="col col-12  ">
          <SimpleSlider />
        </div>
      </div>

      <div className="row  rounded alert alert-secondary ">
        <div className="col  ">
          <MenuPrincipal />
        </div>
        <div className="col col-3 " >
          <Buscador />
        </div>
      </div>



    </div>



  );
}

export default Encabezado;
