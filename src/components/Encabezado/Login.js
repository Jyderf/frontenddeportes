import React, { Component } from 'react';
import { AlertProvider } from 'react-alerts-plus';
import swal from 'sweetalert'



class Login extends Component {

  constructor(argumentos) {
    super(argumentos);

    this.state = {
      email: "", password: "",
    }

    this.onChange = this.onChange.bind(this);
    this.enviarDatosLogin = this.enviarDatosLogin.bind(this);

  }

  onChange(evento) {

    let name = evento.target.name;
    let value = evento.target.value;

    this.setState({
      [name]: value, //podría poner cada uno de las variables nombre1, nombre2, etc pero con esto me ahorro código
    });

    console.log(this.state);

  }

  enviarDatosLogin(evento) {

    evento.preventDefault();

  }



  render() {


    return (


      <div className="container">


        <form action="" onSubmit={this.enviarDatosLogin} className="form-row  col-12 mt-2 " >

          <div className="form-col form-group  ">
            <div className="form-row row-3">

              <div className="form-group col-5 ">
                <input name="email" id="email" type="email"  value={this.state.email} onChange={this.onChange}
                 className="form-control form-control-sm" placeholder="email" required/>
              </div>

              <div className="form-group co-5 ">
                <input name="password" id="password"  value={this.state.password} onChange={this.onChange}
                 type="password" className="form-control form-control-sm" placeholder="Contraseña" required />
              </div>

              <div className="form-group col ">
                <input type="submit" value="Enviar" className="btn btn-outline-success btn-sm form-control form-control-sm" />
              </div>

            </div>


          </div>

        </form>

      </div>
    );
  }

}

export default Login;