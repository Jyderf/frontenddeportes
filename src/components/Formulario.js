import React, { Component } from 'react';
import axios from 'axios';
import Select from 'react-select';
import { MDBContainer, MDBInput } from "mdbreact";


const options = [
    { label: "--seleccione--", value: "--seleccione--", },
    { label: "CC", value: "CC", },
    { label: "TI", value: "CC", },
    { label: "CE", value: "CE", },
    { label: "Otro", value: "Otro", },
];

const gen_sex = [
    { label: "--seleccione--", value: "--seleccione--", },
    { label: "Masculino", value: "Masculino", },
    { label: "Femenino", value: "Femenino", },
];

class Formulario extends Component {

    constructor(props) {
        super(props);
        this.state = {
            identificacion: '', tipo_doc: '', nombre1: '', nombre2: '', apellido1: '', apellido2: '',
            email: '', celular: '', fecha_nac: '', genero: '', username: '', password: ''
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (e) => { this.setState({ [e.target.id]: e.target.value }) };



    handleSubmit = (e) => {

        e.preventDefault();
        console.log(this.state);
        let form_data = new FormData();

        form_data.append('identificacion', this.state.identificacion);
        form_data.append('tipo_doc', this.state.tipo_doc);
        form_data.append('nombre1', this.state.nombre1);
        form_data.append('nombre2', this.state.nombre2);
        form_data.append('apellido1', this.state.apellido1);
        form_data.append('apellido2', this.state.apellido2);
        form_data.append('email', this.state.email);
        form_data.append('celular', this.state.celular);
        form_data.append('fecha_nac', this.state.fecha_nac);
        form_data.append('genero', this.state.genero);
        form_data.append('username', this.state.username);
        form_data.append('password', this.state.password);

        let url = 'http://localhost:8000/api/usuario/';
        axios.post(url, form_data, {
            headers: {
                'content-type': 'multipart/form-data'
            }
        })
            .then(res => {
                console.log(res.data);
            })
            .catch(err => console.log(err))
        //window.location.href = window.location.href;
    };

    render() {
        return (
            <MDBContainer  className="App form-group mt-2 ">
                <form onSubmit={this.handleSubmit}>

                    <span class=" text-info " >Formulario de inscripción</span>
                    <input type="number" placeholder='Número de Identifación' id='identificacion' name='identifacion' value={this.state.identificacion} onChange={this.handleChange}
                        className="form-control my-2 num" required />

                    <div className="form-group">
                        <span class="badge badge-pill badge-dark ">Tipo documento</span>
                        <div className="select-container">
                            <select id="tipo_doc" name="tipo_doc" value={this.state.value} onChange={this.handleChange} className="form-control my-2 num">
                                {options.map((option) => (
                                    <option value={option.value}>{option.label}</option>
                                ))}
                            </select>
                        </div>
                    </div>

                    <input type="text" placeholder='Primer nombre' id='nombre1' name='nombre1' value={this.state.nombre1} onChange={this.handleChange}
                        className="form-control my-2" required />

                    <input type="text" placeholder='Segundo Nombre' id='nombre2' name='nombre2' value={this.state.nombre2} onChange={this.handleChange}
                        className="form-control my-2" required />

                    <input type="text" placeholder='Primer apellido' id='apellido1' name='apellido1' value={this.state.apellido1} onChange={this.handleChange}
                        className="form-control my-2" required />

                    <input type="text" placeholder='Segundo apellildo' id='apellido2' name='apellido2' value={this.state.apellido2} onChange={this.handleChange}
                        className="form-control my-2" required />

                    <input type="mail" placeholder='email' id='email' name='email' value={this.state.email} onChange={this.handleChange}
                        className="form-control my-2" required />

                    <input type="number" placeholder='celular' id='celular' name='celular' value={this.state.celular} onChange={this.handleChange}
                        className="form-control my-2" required />

                    <input type="date" placeholder='Fecha de nacimiento' id='fecha_nac' name='fecha_nac' value={this.state.fecha_nac} onChange={this.handleChange}
                        className="form-control my-2" required />

                    <div className="form-group">
                        <span class="badge badge-pill badge-dark ">Género</span>
                        <div className="select-container">
                            <select id="genero" name="genero" value={this.state.value} onChange={this.handleChange} className="form-control my-2 num">
                                {gen_sex.map((option) => (
                                    <option value={option.value}>{option.label}</option>
                                ))}
                            </select>
                        </div>
                    </div>

                    <input type="text" placeholder='Nombre Usuario' id='username' name='username' value={this.state.username} onChange={this.handleChange}
                        className="form-control my-2" required />

                    <input type="password" placeholder='Contraseña' id='password' name='password' value={this.state.password} onChange={this.handleChange}
                        className="form-control my-2" required />



                    <input type="submit" className="btn btn-primary" />
                </form>
            </MDBContainer >
        );
    }
}

export default Formulario;
