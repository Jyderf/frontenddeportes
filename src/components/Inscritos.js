import React, { useState, useEffect } from "react";
import "../App.css";

//Este componente nos lee una tabla creada en backend, no modificar código

function Inscritos() {

	let [usuarios, setUsuarios] = useState([]);

	useEffect(() => {
		fetch("http://localhost:8000/api/usuario/", {
			method: "GET",
		})
			.then((response) => response.json())
			.then((data) => {
				setUsuarios(data);
			});
	}, []);

	return (
		<div className="table-responsive my-3 ">

			<div className="container table small table-sm table-bordered " >

				<thead class="thead-dark">
					<tr >
						<th>Doc </th>
						<th >Tipo </th>
						<th >Nombres</th>
						<th >Apellidos</th>
						<th >email </th>
						<th >celular </th>
						<th >Nacimiento </th>
						<th >Sexo </th>
						<th >usuario </th>
					</tr>
				</thead>


				{usuarios.map((usuarios, id) => (
					<tbody>
						<tr key={id}>

							<td>{usuarios.identificacion} </td>
							<td >{usuarios.tipo_doc}</td>
							<td >{usuarios.nombre1}<br /> {usuarios.nombre2}</td>
							<td >{usuarios.apellido1}<br />{usuarios.apellido2} </td>
							<td >{usuarios.email} </td>
							<td >{usuarios.celular} </td>
							<td >{usuarios.fecha_nac} </td>
							<td >{usuarios.genero} </td>
							<td >{usuarios.username} </td>

						</tr>
					</tbody>
				))}



			</div>

		</div>



	);

}

export default Inscritos;