import React from 'react';
import Encabezado from "../Encabezado/Encabezado";
import ProxPartidosCuerpo from './ProxPartidosCuerpo';

function ProxPartidos() {
  return (

    <div className="container ">
      <Encabezado />
      <ProxPartidosCuerpo/>
      
    </div>
    

  );
}

export default ProxPartidos;