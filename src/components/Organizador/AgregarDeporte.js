import React from 'react';
import { Link } from 'react-router-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import TimeField from 'react-simple-timefield';
import img_stilo from '../css/img_stilo/hora.css'








function AgregarDeporte() {
 
  return (


    <div className=" col bg-info text-white d-flex flex-column align-items-center">
      <div className="row m-5">
        <h1> Agregar Deporte</h1>
      </div>

      <div className="row m-2">

        <form >
                

          <div className="form-group ">
          <label for="nombre_deporte">Nombre Deporte</label>
            <input type="text" className="form-control" id="nombre_deporte" placeholder="Nombre Deporte" required />
          </div>

          <div className="form-group ">
          <label for="max_integrantes">Máximo integrantes</label>
            <input type="num" className="form-control" id="max_integrantes" placeholder="Máximo de integrantes" required />
          </div>
          <div className="form-group ">
          <label for="cod_deporte">Código Deporte</label>
            <input type="text" className="form-control" id="cod_deporte" placeholder="Código Deporte" required />
          </div>


          

          <div className="form-check">
            <input type="checkbox" className="form-check-input" id="exampleCheck1" />
            <label className="form-check-label" for="exampleCheck1">Check me out</label>
          </div>
          <button type="submit" className="btn btn-primary">Submit</button>
        </form>

      </div>

      <div className="row ">
        <div className="col">

          <ul className="nav">
            <li><Link to="Organizador"> Volver</Link></li>
          </ul>

        </div>
      </div>




    </div>



  );
}

export default AgregarDeporte;