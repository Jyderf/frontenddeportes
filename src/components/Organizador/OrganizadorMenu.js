import React, { useState } from 'react';
import img_stilo from '../css/img_stilo/menuVertical.css'
import { Link } from 'react-router-dom';




const MenuOrganizador = () => {


    return (



        <nav>

            <div className="menu-toggle">
                <h3>Menu</h3>
                <button type="button" id="menu-btn">
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                </button>
            </div>


            <ul id="respMenu" className="ace-responsive-menu" data-menu-style="vertical">
                

                <li>
                    <a href="javascript:;">
                        <i className="fa fa-home" aria-hidden="true"></i>
                        <Link className="title" to="agregarequipo">Agregar equipo</Link>
                    </a>
                </li>

                <li>
                    <a href="javascript:;">
                        <i className="fa fa-cube" aria-hidden="true"></i>
                        <Link className="title" to="vincularjugadores">Vincular jugadores</Link>
                    </a>
                </li>


                <li>
                    <a className="" href="javascript:;">
                        <i className="fa fa-graduation-cap" aria-hidden="true"></i>
                        <Link className="title" to="organizarencuentros">Organizar encuentros</Link>
                    </a>
                </li>

                <li>
                    <a href="javascript:;">
                        <i className="fa fa-heart" aria-hidden="true"></i>
                        <Link className="title" to="agregardeporte">Agregar deporte</Link>
                    </a>
                </li>

                <li className="last ">
                    <a href="javascript:;">
                        <i className="fa fa-envelope" aria-hidden="true"></i>
                        <span className="title">Agregar administrador</span>
                    </a>
                </li>

                <li className="last ">
                    <a href="javascript:;">
                        <i className="fa fa-envelope" aria-hidden="true"></i>
                        <span className="title">Solo administradodres</span>
                    </a>
                </li>
                
            </ul>
        </nav>


    );
}

export default MenuOrganizador;